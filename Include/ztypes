//
// Include/ztypes
//
// Copyright© 2018, 2020 S.A.Norseev. All rights reserved.
//
// Contacts: norseev@gmail.com
// 
// License: see accompanying file LICENSE.txt
//
// Official repository: https://bitbucket.org/Norseev/levclasses/src/master
//

#pragma once

#ifndef ZTYPES_H
#define ZTYPES_H

#include <zstandardc>
#include <zenvironment>

#if Z_COMPILER_GNU && Z_STANDARD_MORE_11
  #include <cstdint>
#else
  #include <stdint.h>
#endif

#include <cstddef>

namespace Z_GLOBAL_NAMESPACE
{
  typedef int8_t  zint8;
  typedef uint8_t zuint8;

  typedef int16_t  zint16;
  typedef uint16_t zuint16;

  typedef int32_t  zint32;
  typedef uint32_t zuint32;

  typedef int64_t  zint64;
  typedef uint64_t zuint64; 

  typedef size_t zuint;

#if defined Z_CAPACITY_32
  typedef zint32 zint;
#elif defined Z_CAPACITY_64
  typedef zint64 zint;
#endif

  typedef bool zbool;

#if defined Z_USE_FLOAT
  typedef float zreal;
#else
  typedef double zreal;
#endif

  typedef char zchar;
  
  typedef char zchar8;

#if defined Z_COMPILER_SUPPORT_CHAR16_TYPE
  typedef char16_t zchar16;
#else
  typedef zuint16  zchar16;
#endif

#if defined Z_COMPILER_SUPPORT_CHAR32_TYPE
  typedef char32_t zchar32;
#else
  typedef zuint32  zchar32;
#endif
}

#endif
